import Foundation

enum LinkingError: Error {
    case nonExistentNode
    case indexOutOfRange
}

enum StackingError: Error {
    case emptyStack
}

class Node<T: Equatable> {
    let value: T
    var next: Node?
    var previous: Node?
    init(value: T) {
        self.value = value
    }
    static func !=(left: Node<T>, right: Node<T>) -> Bool {
        return left.value != right.value
    }
}

class LinkedList<T: Equatable> {
    var listLink: [Node<T>]
    var first: Node<T>? {
        return listLink[0]
    }
    var last: Node<T>? {
        return listLink[listLink.count - 1]
    }
    var length: Int {
        return listLink.count
    }
    init(chain: [Node<T>]) {
        guard !chain.isEmpty else {
            listLink = []
            return
        }
        for index in 0..<chain.count-1 {
            chain[index].next = chain[index+1]
        }
        for index in 1..<chain.count {
            chain[index].previous = chain[index-1]
        }
        listLink = chain
    }
    
    func add(_ item: Node<T>) {
        if listLink.count != 0 {
            let currentEndOfChain = listLink[listLink.count - 1]
            item.previous = currentEndOfChain
            currentEndOfChain.next = item
        }
        listLink.append(item)
    }
    
    func node(at index: Int) throws -> Node<T> {
        if !listLink.isEmpty && index >= 0 && index < listLink.count {
            return listLink[index]
        }
        throw LinkingError.indexOutOfRange
    }
    
    func remove(node: Node<T>) throws -> T {
        let nodesToRemain = listLink.filter({$0 != node})
        guard listLink.count > nodesToRemain.count else {
            throw LinkingError.nonExistentNode
        }
        listLink = nodesToRemain
        return node.value;
    }
}

struct Stack<T:Equatable> {
    var linkedList: LinkedList<T>
    var size: Int {
        return linkedList.length
    }
    var peek: Node<T>? {
        if let topNode = linkedList.last {
            return topNode
        }
        return nil
    }
    init(chain: LinkedList<T>) {
        linkedList = chain
    }
    init() {
        let initialiserChain = LinkedList<T>(chain:[])
        linkedList = initialiserChain
    }
    func push(_ newNode: Node<T>) {
        linkedList.add(newNode)
    }
    func pop() throws -> T {
        guard let topNode = linkedList.last else {
            throw StackingError.emptyStack
        }
        return try linkedList.remove(node: topNode)
    }
}

extension Node: CustomStringConvertible {
    var description: String {
        "[\(value)]"
    }
}

extension LinkedList: CustomStringConvertible {
    var description: String {
        return "H-\(listLink)-T".replacingOccurrences(of: ", ", with: "-").replacingOccurrences(of: "[[", with: "[").replacingOccurrences(of: "]]", with: "]")
    }
}

extension Stack: CustomStringConvertible {
    var description: String {
        return "\(linkedList)".replacingOccurrences(of: "H-", with : "").replacingOccurrences(of: "-T", with: "").replacingOccurrences(of: "]-[", with: ", ")
    }
}
var newNode = Node<Int>(value: 45)
var newList = LinkedList<Int>(chain: [Node(value: 3), Node(value: 8), Node(value: 12)])
var newStack = Stack<Int>(chain: newList)
newStack.push(newNode)
newStack.push(Node(value: 76))
do {
    try newStack.pop()
} catch {
    print("The stack is empty")
}
print(newStack)
print(newList.last ?? "Nothing to see here")
print(newList.first?.next ?? "No one further along the line")
print(newList.length)
var newerList = LinkedList<String>(chain: [])
var newerStack = Stack<String>(chain: newerList)
var newestStack = Stack<Double>()
print(newerStack)
